# Fetch

Quelques exercices pour comprendre l'API Fetch et les fonctions de *callback*.

# Exercice 1
Afficher dans la console du navigateur le HTML de l'url [https://www.google.com] et le status code de la réponse.

# Exercice 2
Utiliser la fonction *filter* pour créer un tableau de nombres pairs.

```javascript
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

function filter(number) {
    return number % 2 == 0;
}
```

# Exercice 3
Utiliser la variable *filter* pour créer un tableau de nombres pairs.

```javascript
let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

let filter = function(number) {
    return number % 2 == 0;
}
```

# Exercice 4
Utiliser la fonction *displayApiData* avec l'API Fetch pour afficher dans la console de votre navigateur les données retournées par les services web :
* [https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=arrets-et-horaires-theoriques-bus]
* [https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=parkings-enclos-ou-ouvrage-a-chambery&facet=delegataire&facet=secteur&facet=type_park&facet=remarque]

```javascript
function displayApiData(json) {
    console.log(json);
}
```

# Exercice 5 (bonus)
Utiliser l'API Fetch pour afficher la photo de votre tortionnaire préféré dans votre page HTML ;-)

[https://simplon.co/wp-content/uploads/2018/11/IMG_Pierre.jpg]